# Passbolt-Setup

This repository contains scripts to easily setup passbolt via package on:

* Debian 10 / 11
* Raspbian (Raspberry Pi)
* Ubuntu 20.04 / 22.04
* CentOS 7
* Red Hat 7 / 8 / 9
* RockyLinux 8 / 9
* AlmaLinux 8 / 9
* Oracle Linux 8 / 9
* OpenSUSE 15
* Fedora 34 / 35 / 36

![passbolt-setup-video](passbolt-setup.gif)

`passbolt-setup` scripts are provided "as-is" and are not part of the passbolt project.

I wrote these scripts following the installation instructions provided on passbolt [help site](https://help.passbolt.com/hosting/install). Use them on a fresh vanilla server and not on one who is already hosting other services.

# How to use

Create a fresh Linux VM with your favorite OS and ensure your user is sudoers.

Grab the project and go to the downloaded folder:

```
curl https://gitlab.com/AnatomicJC/passbolt-setup/-/archive/main/passbolt-setup-main.tar.gz | tar xzf -
cd passbolt-setup-main
```

From there, edit [passbolt.conf](passbolt.conf), set your passbolt domain name (FQDN variable) and review other options.

Then launch `passbolt-setup.bash` script to see available options:

```bash
./passbolt-setup.bash
  Usage:
    ./passbolt-setup.bash ce
      Will setup Passbolt CE version

    ./passbolt-setup.bash pro
      Will setup Passbolt PRO version, you must set a subscription_key.txt file

    ./passbolt-setup.bash import-data
      Will import Passbolt demo data

    ./passbolt-setup.bash purge
      Will remove Passbolt

    ./passbolt-setup.bash help
      Display this help
```

passbolt-setup script will install all needed dependencies, and generate a **passbolt.php** file according to your defined variables in **[passbolt.conf](passbolt.conf)**.

It ends with a direct link to let you generate your private key and setup your passphrase.

```
     ____                  __          ____
    / __ \____  _____ ____/ /_  ____  / / /_
   / /_/ / __ `/ ___/ ___/ __ \/ __ \/ / __/
  / ____/ /_/ (__  |__  ) /_/ / /_/ / / /
 /_/    \__,_/____/____/_.___/\____/_/\__/

 Open source password manager for teams
-------------------------------------------------------------------------------
User saved successfully.
To start registration follow the link provided in your mailbox or here:
https://ce.ubuntu2004.jc/setup/install/506ac077-3924-4802-85be-cc27c2bbeb5e/b3db9de0-71e4-459a-a4ba-e7df4642c26b
```

# Good to know

## SSL config

If you want to use your own certificates, put them in the certs folder and name them as `cert.pem` and `key.pem`.

You can also use Let's Encrypt, as explained in [passbolt.conf](passbolt.conf) file.

## Passbolt PRO

You will need a valid subscription key to be able to setup Passbolt PRO. Create a subscription_key.txt file at the root folder of this repository containing your valid Passbolt PRO subscription key and run `bash passbolt-setup.bash pro`

## MailHog

You must provide SMTP settings to make passbolt send notification and account creation emails. If you want to setup passbolt just for testing purposes, you can use Mailhog, a SMTP daemon who will catch all emails. Let the `MAILHOG_ENABLE` option to true to enable it and mailhog will be available on https://your.passbolt.url/webmail/ (don't forget the trailing slash if you are using the vagrant file).

## Demo data

passbolt-setup.bash script provides demo data. Run `bash passbolt-setup.bash import-data` and follow instructions. Be warned that this command will **drop your database** and replace your current instance with demo data.

## Vagrant

The provided Vagrantfile is just for spin up a fresh VM and play with these scripts. Feel free to replace Debian 11 [with your favorite OS](https://app.vagrantup.com/boxes/search) !

[passbolt.conf.vagrant](passbolt.conf.vagrant) file contains overrided variables.

## Tips

To override default options, you can create a passbolt.conf.override file.
