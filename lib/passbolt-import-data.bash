#!/usr/bin/env bash

set -euo pipefail 

zcat data/passbolt-demo.sql.gz | sudo mysql "${MYSQL_DATABASE_NAME}"

sudo -EH -u "${WEBUSER}" bash -c "/usr/share/php/passbolt/bin/cake passbolt migrate"
sudo -EH -u "${WEBUSER}" bash -c "/usr/share/php/passbolt/bin/cake cache clear_all"
