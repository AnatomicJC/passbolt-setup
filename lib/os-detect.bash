#!/usr/bin/env bash

if [ -f /etc/debian_version ]
then
    PACKAGE_MANAGER=apt
    export DEBIAN_RECOMMENDS=''
    DISTRONAME=$(grep -E "^ID=" /etc/os-release | awk -F= '{print $2}')
    # CODENAME used for Debian family
    CODENAME=$(grep -E "^VERSION_CODENAME=" /etc/os-release | awk -F= '{print $2}' || true)

    # Handle Raspberry PI raspbian OS
    if [ "${DISTRONAME}" = "raspbian" ]
    then
        DISTRONAME="debian"
    fi
    # We use buster debian package for bullseye
    if [ "${CODENAME}" = "bullseye" ]
    then
        CODENAME="buster"
    # We use focal ubuntu package for jammy
    elif [ "${CODENAME}" = "jammy" ]
    then
        CODENAME="focal"
    fi

elif which zypper > /dev/null 2>&1
then
    PACKAGE_MANAGER=zypper
elif which dnf > /dev/null 2>&1
then
    PACKAGE_MANAGER=dnf
elif which yum > /dev/null 2>&1
then
    PACKAGE_MANAGER=yum
else
    echo "Can't find compatible operating system"
    echo "Exit"
    exit 1
fi

# RHEL Family get OS major version (7, 8, 9)
if [ "${PACKAGE_MANAGER}" = "yum" ] || [ "${PACKAGE_MANAGER}" = "dnf" ] || [ "${PACKAGE_MANAGER}" = "zypper" ]
then
    if [ "${PACKAGE_MANAGER}" = "zypper" ]
    then
      CLEAN_PARAM="--"
    fi
    sudo ${PACKAGE_MANAGER} clean ${CLEAN_PARAM:-}all > /dev/null
    export OS_NAME="${ID}"
    export OS_VERSION="${VERSION_ID}"
    export OS_VERSION_MAJOR="${VERSION_ID%.*}"

    # RHEL 8 and 9 share the same package from same repository
    # there is no RHEL9 package build
    export RPM_REPO_VERSION="${OS_VERSION_MAJOR}"
    if [ "${OS_VERSION_MAJOR}" = 9 ]
    then
      export RPM_REPO_VERSION="8"
    fi
fi

# Web User
if [ "${PACKAGE_MANAGER}" = "apt" ]
then
  export WEBUSER="www-data"
elif [ "${PACKAGE_MANAGER}" = "zypper" ]
then
  export WEBUSER="wwwrun"
else
  export WEBUSER="nginx"
fi
