#!/usr/bin/env bash

if [ -n "${ARGUMENT}" ]
then
  if [ "${ARGUMENT}" = "ce" ]
  then
    PASSBOLT_FLAVOUR=ce
  elif [ "${ARGUMENT}" = "pro" ]
  then
    PASSBOLT_FLAVOUR=pro
  elif [ "${ARGUMENT}" = "purge" ]
  then
    remove_and_purge_passbolt
    exit 0
  elif [ "${ARGUMENT}" = "import-data" ]
  then
    # shellcheck source=/dev/null
    . lib/passbolt-import-data.bash
    display_passbolt_demo_infos
    exit 0
  elif [ "${ARGUMENT}" = "import-data-dev" ]
  then
    # shellcheck source=/dev/null
    . lib/passbolt-import-data-dev.bash
    display_passbolt_demo_infos
    exit 0
  elif [ "${ARGUMENT}" = "help" ]
  then
    display_help
  else
    display_help
  fi
else
  display_help
fi

if [ -z ${FQDN+x} ]
then
  DOMAIN=$(hostname -s).jc
  FQDN=${PASSBOLT_FLAVOUR}.${DOMAIN}
fi
