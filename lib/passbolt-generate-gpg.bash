#!/usr/bin/env bash

set -eo pipefail

PASSBOLT_CONFIG_PATH="/etc/passbolt"
GNUPGHOME=/var/lib/passbolt/.gnupg

gpg_private_key="${PASSBOLT_GPG_SERVER_KEY_PRIVATE:-$PASSBOLT_CONFIG_PATH/gpg/serverkey_private.asc}"
gpg_public_key="${PASSBOLT_GPG_SERVER_KEY_PUBLIC:-$PASSBOLT_CONFIG_PATH/gpg/serverkey.asc}"
key_email="${PASSBOLT_KEY_EMAIL:-admin@passbolt.com}"
key_name="${PASSBOLT_KEY_NAME:-Passbolt Admin}"
key_length="${PASSBOLT_KEY_LENGTH:-4096}"
subkey_length="${PASSBOLT_SUBKEY_LENGTH:-4096}"
expiration="${PASSBOLT_KEY_EXPIRATION:-0}"

KEY_TYPE="rsa"
SUBKEY_TYPE="rsa"
_GPG_NO_PROTECTION="%no-protection"
_KEY_USAGE="Key-Usage: sign,cert"
_SUBKEY_USAGE="SubKey-Usage: encrypt"

if [ "${PACKAGE_MANAGER}" = "yum" ]
then
  # This option doesn't exists in gpg packaged on RHEL 7
  _GPG_NO_PROTECTION=""
  KEY_TYPE="default"
  SUBKEY_TYPE="default"
  _KEY_USAGE=""
  _SUBKEY_USAGE=""
fi

entropy_check() {
  local entropy_avail

  entropy_avail=$(cat /proc/sys/kernel/random/entropy_avail)

  if [ "$entropy_avail" -lt 2000 ]; then

    cat <<EOF
==================================================================================
  Your entropy pool is low. This situation could lead GnuPG to not
  be able to create the gpg serverkey so the container start process will hang
  until enough entropy is obtained.
  Please consider installing rng-tools and/or virtio-rng on your host as the
  preferred method to generate random numbers using a TRNG.
  If rngd (rng-tools) does not provide enough or fast enough randomness you could
  consider installing haveged as a helper to speed up this process.
  Using haveged as a replacement for rngd is not recommended. You can read more
  about this topic here: https://lwn.net/Articles/525459/
==================================================================================
EOF
  fi
}

gpg_gen_key() {

  entropy_check

  sudo su -c "gpg --homedir $GNUPGHOME --batch --no-tty --gen-key <<EOF
    Key-Type: ${KEY_TYPE}
    Key-Length: $key_length
    ${_KEY_USAGE}
    Subkey-Type: ${SUBKEY_TYPE}
    Subkey-Length: $subkey_length
    ${_SUBKEY_USAGE}
    Name-Real: $key_name
    Name-Email: $key_email
    Expire-Date: $expiration
    ${_GPG_NO_PROTECTION}
    %commit
EOF" -ls /bin/bash "${WEBUSER}"

  sudo su -c "gpg --homedir ${GNUPGHOME} --armor --export-secret-keys $key_email > $gpg_private_key" -ls /bin/bash "${WEBUSER}"
  sudo su -c "gpg --homedir ${GNUPGHOME} --armor --export $key_email > $gpg_public_key" -ls /bin/bash "${WEBUSER}"
}

gpg_import_key() {
  sudo su -c "gpg --homedir ${GNUPGHOME} --batch --import $gpg_public_key" -ls /bin/bash "${WEBUSER}"
  sudo su -c "gpg --homedir ${GNUPGHOME} --batch --import $gpg_private_key" -ls /bin/bash "${WEBUSER}"
}

sudo su -c "rm -rf /var/lib/passbolt/.gnupg " -ls /bin/bash "${WEBUSER}"
sudo mkdir -p /etc/passbolt/gpg
sudo chown -R "${WEBUSER}": /etc/passbolt/gpg
gpg_gen_key
#gpg_import_key
