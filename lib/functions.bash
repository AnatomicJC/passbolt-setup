#!/usr/bin/env bash

display_help() {
cat << EOF
  Usage:
    ./passbolt-setup.bash ce
      Will setup Passbolt CE version

    ./passbolt-setup.bash pro
      Will setup Passbolt PRO version, you must set a subscription_key.txt file

    ./passbolt-setup.bash import-data
      Will import Passbolt demo data

    ./passbolt-setup.bash purge
      Will remove Passbolt

    ./passbolt-setup.bash help
      Display this help
EOF
exit 0
}

check_subscription_key () {
if [ ! -f "${SCRIPT_DIR}"/subscription_key.txt ] && [ "${PASSBOLT_FLAVOUR}" = "pro" ]
then
  cat << EOF
You need a subscription key to set a pro version
EOF
  exit 1
fi
}

copy_subscription_key () {
  if [ -f "${SCRIPT_DIR}"/subscription_key.txt ] && [ "${PASSBOLT_FLAVOUR}" = "pro" ]
  then
    sudo cp subscription_key.txt /etc/passbolt/subscription_key.txt
  fi
}

configure_first_admin () {
  sudo -EH -u "${WEBUSER}" bash -c "/usr/share/php/passbolt/bin/cake passbolt install --no-admin"
  sudo -EH -u "${WEBUSER}" bash -c "/usr/share/php/passbolt/bin/cake passbolt register_user -u ${PASSBOLT_FIRST_ADMIN_EMAIL} -f ${PASSBOLT_FIRST_ADMIN_USERNAME} -l ${PASSBOLT_FIRST_ADMIN_SURNAME} -r admin"
}

configure_ssl_keys () {
  export SCHEME=http
  if [ -f "${SSL_CERT_PATH}" ] && [ -f "${SSL_KEY_PATH}" ]
  then
    # SSL
    SCHEME=https
    sudo mkdir -p /etc/nginx/ssl
    sudo cp "${SSL_CERT_PATH}" /etc/nginx/ssl/passbolt-cert.pem
    sudo cp "${SSL_KEY_PATH}" /etc/nginx/ssl/passbolt-key.pem
    sudo chmod 400 /etc/nginx/ssl/passbolt-key.pem
  fi
}

_error_exit () {
  MESSAGE=${1:-Unknown error}
  echo "${MESSAGE}"
  echo "Exit"
  exit 1
}

function is_supported_distro() {
    local DISTROS=(
            "debian10"
            "debian11"
            "raspbian"
            "ubuntu20"
            "ubuntu22"
            "centos7"
            "rhel7"
            "rhel8"
            "rhel9"
            "rocky8"
            "rocky9"
            "ol8"
            "ol9"
            "almalinux8"
            "almalinux9"
            "fedora34"
            "fedora35"
            "fedora36"
            "opensuse-leap15"
          )
    for DISTRO in "${DISTROS[@]}"
    do
      # the ${VERSION_ID%.*} pattern is to remove minor version, aka rhel8 for rhel8.6
      [[ "${ID}${VERSION_ID%.*}" = ${DISTRO}* ]] && return 0
    done
    return 1
}

compliance_check () {
  local NOT_SUPPORTED_DISTRO="Unfortunately, ${PRETTY_NAME:-This Linux distribution} is not supported :-("
  if ! is_supported_distro; then
    _error_exit "${NOT_SUPPORTED_DISTRO}"
  fi
  local IPV6_ERROR="Your server has no IPv6 support"
  if ! sudo sysctl -a | grep disable_ipv6 > /dev/null
  then
    _error_exit "${IPV6_ERROR}"
  fi
  local PHP_ERROR="PHP is already installed, you must execute this script on a vanilla server"
  if [ -f /usr/bin/dpkg ]
  then
    if dpkg -l | grep php > /dev/null
    then
      _error_exit "${PHP_ERROR}"
    fi
  fi
  if [ -f /usr/bin/rpm ]
  then
    if rpm -qa | grep php > /dev/null
    then
      _error_exit "${PHP_ERROR}"
    fi
    if rpm -qa | grep remi-release > /dev/null 
    then
      _error_exit "remi-release is already installed, please remove it before executing this script"
    fi
  fi
}

install_dependencies () {
  if [ "${PACKAGE_MANAGER}" = "apt" ]
  then
    sudo "${PACKAGE_MANAGER}" update
    sudo "${PACKAGE_MANAGER}" install -y \
        apt-transport-https \
        ca-certificates \
        curl \
        gnupg-agent \
        software-properties-common \
        haveged \
        certbot \
        wget \
        python3-certbot-nginx
  elif [ "${PACKAGE_MANAGER}" = "zypper" ]
  then
    cat << EOF | sudo tee /etc/zypp/repos.d/php.repo > /dev/null
[php]
enabled=1
autorefresh=0
baseurl=http://download.opensuse.org/repositories/devel:/languages:/php/openSUSE_Leap_${OS_VERSION}/
EOF
    PHP_EXTENSION_REPO_VERSION="openSUSE_Leap_${OS_VERSION}"
    if [ "${OS_VERSION}" = "15.4" ]
    then
      PHP_EXTENSION_REPO_VERSION="15.4"
    fi
    cat << EOF | sudo tee /etc/zypp/repos.d/php-extensions-x86_64.repo > /dev/null
[php-extensions-x86_64]
enabled=1
autorefresh=0
baseurl=http://download.opensuse.org/repositories/server:/php:/extensions/${PHP_EXTENSION_REPO_VERSION}/
EOF
  elif [ "${OS_NAME}" = "fedora" ]
  then
    if ! rpm -qa | grep remi-release > /dev/null
    then
      sudo "${PACKAGE_MANAGER}" install -y https://rpms.remirepo.net/fedora/remi-release-"${OS_VERSION}".rpm
    fi
    sudo "${PACKAGE_MANAGER}" install -y dnf-plugins-core wget
    sudo "${PACKAGE_MANAGER}" module reset php -y
    sudo "${PACKAGE_MANAGER}" module install php:remi-8.1 -y
    sudo "${PACKAGE_MANAGER}" config-manager --set-enabled remi
    # pcre2 package needs to be upgraded to last version
    # there is a bug with preg_match() if we keep the current one installed
    sudo "${PACKAGE_MANAGER}" clean all
    sudo "${PACKAGE_MANAGER}" upgrade -y pcre2
  elif [ "${PACKAGE_MANAGER}" = "yum" ] || [ "${PACKAGE_MANAGER}" = "dnf" ]
  then
    if [ "$(grep -E "^ID=" /etc/os-release | awk -F= '{print $2}' | sed 's/"//g')" = "ol" ]
    then
      # Oracle Linux
      sudo "${PACKAGE_MANAGER}" install -y oracle-epel-release-el"${OS_VERSION_MAJOR}" wget
    else
      sudo "${PACKAGE_MANAGER}" install -y epel-release wget
    fi
    sudo "${PACKAGE_MANAGER}" install -y https://rpms.remirepo.net/enterprise/remi-release-"${OS_VERSION_MAJOR}".rpm
    if [ "${OS_VERSION_MAJOR}" -eq 7 ]
    then
      sudo "${PACKAGE_MANAGER}" install -y yum-utils
      sudo yum-config-manager --disable 'remi-php*'
      sudo yum-config-manager --enable   remi-php81
    else
      sudo "${PACKAGE_MANAGER}" module -y reset php
      sudo "${PACKAGE_MANAGER}" module -y install php:remi-8.1
    fi
  fi
}

pull_updated_pub_key() {
  declare -a serverlist=("keys.mailvelope.com" "keys.openpgp.org" "pgp.mit.edu")
  for serverin in "${serverlist[@]}"
  do
    if ! sudo test -d /root/.gnupg
    then
      sudo mkdir -m 0700 /root/.gnupg
    fi
    # Handle gpg error in case of a server key failure
    # Without this check, and because we are using set -euo pipefail
    # The script fail in case of failure
    if sudo gpg --no-default-keyring --keyring "${PASSBOLT_KEYRING_FILE}" --keyserver hkps://"${serverin}" --recv-keys "${PASSBOLT_FINGERPRINT}"; then
      break
    fi
  done
}

setup_repository () {
  if [ "${PACKAGE_MANAGER}" = "apt" ]
  then
    pull_updated_pub_key > /dev/null
    sudo chmod 644 "${PASSBOLT_KEYRING_FILE}"

    sudo rm -f /etc/apt/sources.list.d/passbolt.list
    cat << EOF | sudo tee /etc/apt/sources.list.d/passbolt.sources > /dev/null
Types: deb
URIs: https://download.passbolt.com/${PASSBOLT_FLAVOUR}/${DISTRONAME}
Suites: ${CODENAME}
Components: ${PASSBOLT_BRANCH}
Signed-By: ${PASSBOLT_KEYRING_FILE}
EOF
    sudo apt update
  elif [ "${OS_NAME}" = "fedora" ]
  then
    cat << EOF | sudo tee /etc/yum.repos.d/passbolt.repo > /dev/null
[passbolt-server]
name=Passbolt Server
baseurl=https://download.passbolt.com/${PASSBOLT_FLAVOUR}/rpm/el8/${PASSBOLT_BRANCH}
enabled=1
gpgcheck=0
EOF
  elif [ "${PACKAGE_MANAGER}" = "zypper" ]
  then
    cat << EOF | sudo tee /etc/zypp/repos.d/passbolt.repo > /dev/null
[passbolt-server]
name=Passbolt Server
baseurl=https://download.passbolt.com/${PASSBOLT_FLAVOUR}/rpm/opensuse/${PASSBOLT_BRANCH}
enabled=1
gpgcheck=0
EOF
  elif [ "${PACKAGE_MANAGER}" = "yum" ] || [ "${PACKAGE_MANAGER}" = "dnf" ]
  then
    cat << EOF | sudo tee /etc/yum.repos.d/passbolt.repo > /dev/null
[passbolt-server]
name=Passbolt Server
baseurl=https://download.passbolt.com/${PASSBOLT_FLAVOUR}/rpm/el${RPM_REPO_VERSION}/${PASSBOLT_BRANCH}
enabled=1
gpgcheck=1
gpgkey=https://download.passbolt.com/pub.key
EOF
  fi
  # Add MariaDB 10.5 repository for CentOS 7
  if [ "${PACKAGE_MANAGER}" = "yum" ]
  then
    cat << EOF | sudo tee /etc/yum.repos.d/mariadb.repo > /dev/null
[mariadb]
name = MariaDB
baseurl = http://yum.mariadb.org/10.8/centos7-amd64
gpgkey=https://yum.mariadb.org/RPM-GPG-KEY-MariaDB
gpgcheck=1
EOF
  fi
}

install_passbolt () {
  if [ "${PACKAGE_MANAGER}" = "apt" ]
  then
    if [ "${MYSQL_ENABLED}" = 'true' ]
    then
      echo passbolt-"${PASSBOLT_FLAVOUR}"-server passbolt/mysql-configuration boolean "${MYSQL_ENABLED}" | sudo debconf-set-selections
      echo passbolt-"${PASSBOLT_FLAVOUR}"-server passbolt/mysql-passbolt-username string "${MYSQL_USERNAME}" | sudo debconf-set-selections
      echo passbolt-"${PASSBOLT_FLAVOUR}"-server passbolt/mysql-passbolt-password password "${MYSQL_PASSWORD}" | sudo debconf-set-selections
      echo passbolt-"${PASSBOLT_FLAVOUR}"-server passbolt/mysql-passbolt-password-repeat password "${MYSQL_PASSWORD}" | sudo debconf-set-selections
      echo passbolt-"${PASSBOLT_FLAVOUR}"-server passbolt/mysql-passbolt-dbname string "${MYSQL_DATABASE_NAME}" | sudo debconf-set-selections
    fi

    if [ "${NGINX_ENABLED}" = 'true' ]
    then
      echo passbolt-"${PASSBOLT_FLAVOUR}"-server passbolt/nginx-configuration boolean "${NGINX_ENABLED}" | sudo debconf-set-selections
      echo passbolt-"${PASSBOLT_FLAVOUR}"-server passbolt/nginx-configuration-three-choices select "${NGINX_SCHEME_METHOD}" | sudo debconf-set-selections
      echo passbolt-"${PASSBOLT_FLAVOUR}"-server passbolt/nginx-configuration-two-choices select "${NGINX_SCHEME_METHOD}" | sudo debconf-set-selections
      echo passbolt-"${PASSBOLT_FLAVOUR}"-server passbolt/nginx-domain string "${FQDN}" | sudo debconf-set-selections
    fi

    if [ -f "${SSL_CERT_PATH}" ] && [ -f "${SSL_KEY_PATH}" ]
    then
      echo passbolt-"${PASSBOLT_FLAVOUR}"-server passbolt/nginx-certificate-file string /etc/nginx/ssl/passbolt-cert.pem | sudo debconf-set-selections
      echo passbolt-"${PASSBOLT_FLAVOUR}"-server passbolt/nginx-certificate-key-file string /etc/nginx/ssl/passbolt-key.pem | sudo debconf-set-selections
    fi

    if [ "${MYSQL_ENABLED}" != 'true' ]
    then
      DEBIAN_RECOMMENDS='--no-install-recommends'
    fi
    sudo DEBIAN_FRONTEND=noninteractive "${PACKAGE_MANAGER}" install -y ${DEBIAN_RECOMMENDS}  passbolt-"${PASSBOLT_FLAVOUR}"-server
  elif [ "${PACKAGE_MANAGER}" = "yum" ] || [ "${PACKAGE_MANAGER}" = "dnf" ] || [ "${PACKAGE_MANAGER}" = "zypper" ]
  then
    if [ "${PACKAGE_MANAGER}" = "zypper" ]
    then
      sudo "${PACKAGE_MANAGER}" -n --no-gpg-checks install -y passbolt-"${PASSBOLT_FLAVOUR}"-server
    else
      sudo "${PACKAGE_MANAGER}" install -y passbolt-"${PASSBOLT_FLAVOUR}"-server
    fi
    if [ -f "${SSL_CERT_PATH}" ] && [ -f "${SSL_KEY_PATH}" ]
    then
      sudo /usr/local/bin/passbolt-configure \
        -P "${MYSQL_ROOT_PASSWORD}" \
        -p "${MYSQL_PASSWORD}" \
        -d "${MYSQL_DATABASE_NAME}" \
        -u "${MYSQL_USERNAME}" \
        -H "${FQDN}" \
        -c "${SCRIPT_DIR}"/certs/cert.pem \
        -k "${SCRIPT_DIR}"/certs/key.pem \
        -e
    else
      sudo /usr/local/bin/passbolt-configure \
        -P "${MYSQL_ROOT_PASSWORD}" \
        -p "${MYSQL_PASSWORD}" \
        -d "${MYSQL_DATABASE_NAME}" \
        -u "${MYSQL_USERNAME}" \
        -H "${FQDN}" \
        -e
    fi
  fi
}

remove_and_purge_passbolt () {
  if [ "${DISPLAY_INSTALL_RESUME}" = "true" ] && [[ ${ARGUMENT} =~ ^purge$ ]]
  then
    cat << EOF
!!! DANGER !!!

You are about to remove Passbolt, but not only...

By answering yes, this script will:

  * uninstall nginx / php / mariadb / mysql and remove all configuration files
  * drop all databases

If you have other databases or softwares running on this server, surely that's not what you want.

EOF
    do_you_want_to_continue
  fi

  if [ "${PACKAGE_MANAGER}" = "apt" ]
  then
    echo mariadb-server-10.3 mariadb-server-10.3/postrm_remove_databases boolean true | sudo debconf-set-selections
    echo mariadb-server-10.5 mariadb-server-10.5/postrm_remove_databases boolean true | sudo debconf-set-selections
    echo mariadb-server-10.6 mariadb-server-10.6/postrm_remove_databases boolean true | sudo debconf-set-selections
    echo mysql-server-8.0 mysql-server-8.0/postrm_remove_databases boolean true | sudo debconf-set-selections
    # shellcheck disable=SC2046
    sudo apt remove --purge -y $(dpkg -l | grep -E "(passbolt|nginx|mysql|mariadb|php)" | awk '{print $2}')
    sudo apt autoremove --purge -y
    sudo rm -f /etc/apt/sources.list.d/passbolt.list
    sudo rm -f /etc/apt/sources.list.d/passbolt.sources
    sudo rm -f "${PASSBOLT_KEYRING_FILE}"*
    sudo apt-get update
  elif [ "${PACKAGE_MANAGER}" = "yum" ] || [ "${PACKAGE_MANAGER}" = "dnf" ] || [ "${PACKAGE_MANAGER}" = "zypper" ]
  then
    # shellcheck disable=SC2046
    sudo "${PACKAGE_MANAGER}" remove -y $(rpm -qa --qf "%{NAME}\n" | grep -iE "(passbolt|nginx|mysql|mariadb|php|remi)")
    if [ "${PACKAGE_MANAGER}" = "zypper" ]
    then
      sudo rm -f /etc/zypp/repos.d/passbolt.repo
      sudo rm -f /etc/zypp/repos.d/php.repo
      sudo rm -f /etc/zypp/repos.d/php-extensions-x86_64.repo
    else
      sudo rm -f /etc/yum.repos.d/passbolt.repo
    fi
  fi

  if [ -f /etc/systemd/system/mailhog.service ]
  then
    sudo systemctl stop mailhog.service
    sudo rm -f /etc/systemd/system/mailhog.service
    sudo systemctl daemon-reload
  fi

  if [ -f /usr/local/bin/mailhog ]
  then
    sudo rm -f /usr/local/bin/mailhog
  fi

  sudo rm -rf /usr/share/php/passbolt/
  # Databases are not purged on Ubuntu :-/
  sudo rm -rf /var/lib/mysql
  sudo rm -rf /etc/passbolt
  sudo rm -rf /var/lib/passbolt
}

do_you_want_to_continue () {
  while true; do
      read -r -p "Do you wish to continue? (yes/no) " yn
      case $yn in
          [Yy]* ) break;;
          [Nn]* ) exit;;
          * ) echo "Please answer yes or no.";;
      esac
  done
}

display_passbolt_demo_infos () {
  cat << EOF

Caveats and conditions:

Your Passbolt instance has been populated with testing data.
This is provided "as-is" and is NOT intended for production use.

-------------------------------------------------------------------------------

Connect to Passbolt and recover one of these email accounts:

 * ada@passbolt.dev
 * betty@passbolt.dev
 * admin@passbolt.dev

The email cronjob is executed each minute, so  wait for the recovery account email.

You will find a link to recover your account who will redirect you to a page asking for you OpenPGP private key block.

Here are the OpenPGP key weblinks:

* ada@passbolt.dev: https://raw.githubusercontent.com/anatomicjc/passbolt-test-data/master/config/gpg/ada_private.key
* betty@passbolt.dev: https://raw.githubusercontent.com/anatomicjc/passbolt-test-data/master/config/gpg/betty_private.key
* admin@passbolt.dev: https://raw.githubusercontent.com/anatomicjc/passbolt-test-data/master/config/gpg/admin_private.key

You will then prompted for a passphrase, who is the email address of the account you are trying to recover.

EOF
}
