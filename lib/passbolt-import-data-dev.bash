#!/usr/bin/env bash

set -euo pipefail 

sudo "${PACKAGE_MANAGER}" install -y git zip

sudo ln -sf /etc/passbolt /usr/share/php/passbolt/config

if sudo test -f /etc/passbolt/ldap.default.php
then
  # PRO
  sudo curl -o /usr/share/php/passbolt/composer.json https://bitbucket.org/passbolt_pro/passbolt_pro_api/raw/master/composer.json
  sudo curl -o /usr/share/php/passbolt/composer.lock https://bitbucket.org/passbolt_pro/passbolt_pro_api/raw/master/composer.lock
else
  # CE
  sudo curl -o /usr/share/php/passbolt/composer.json https://raw.githubusercontent.com/passbolt/passbolt_api/master/composer.json
  sudo curl -o /usr/share/php/passbolt/composer.lock https://raw.githubusercontent.com/passbolt/passbolt_api/master/composer.lock
fi

sudo mkdir -p /var/www/.cache/composer/files/
sudo chown -R "${WEBUSER}": /var/www/.cache

sudo mkdir -p /usr/share/php/passbolt/vendor
sudo chown -R "${WEBUSER}": /usr/share/php/passbolt/vendor
sudo chown -R "${WEBUSER}": /usr/share/php/passbolt
sudo chown -R "${WEBUSER}": /etc/passbolt

grep -qxF 'DEBUG=true' /etc/environment || echo DEBUG=true | sudo tee -a /etc/environment
grep -qxF 'PASSBOLT_SELENIUM_ACTIVE=true' /etc/environment || echo PASSBOLT_SELENIUM_ACTIVE=true | sudo tee -a /etc/environment

while read -r line;
do
  # shellcheck disable=SC2163
  export "${line}"
done < /etc/environment

cd /usr/share/php/passbolt/

EXPECTED_CHECKSUM="$(php -r 'copy("https://composer.github.io/installer.sig", "php://stdout");')"
sudo php -r "copy('https://getcomposer.org/installer', 'composer-setup.php');"
ACTUAL_CHECKSUM="$(php -r "echo hash_file('sha384', 'composer-setup.php');")"

if [ "$EXPECTED_CHECKSUM" != "$ACTUAL_CHECKSUM" ]
then
    >&2 echo 'ERROR: Invalid installer checksum'
    sudo rm composer-setup.php
    exit 1
fi

sudo php composer-setup.php
sudo rm composer-setup.php

sudo sed -i 's!passbolt/passbolt-test-data!anatomicjc/passbolt-test-data!g' /usr/share/php/passbolt/composer.json
sudo sed -i 's!\(^[[:space:]]*"anatomicjc/passbolt-test-data": "\)\(\^3.2.0\)\(.*\)!\1dev-master\3!g' composer.json
sudo runuser -u "${WEBUSER}" -- php composer.phar update -n
sudo runuser -u "${WEBUSER}" -- php composer.phar install -n

sudo -EH -u "${WEBUSER}" bash -c "/usr/share/php/passbolt/bin/cake passbolt insert default"