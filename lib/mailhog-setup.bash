#!/usr/bin/env bash

set -eo pipefail

echo "Setup MailHog"

sudo wget --quiet -O /usr/local/bin/mailhog https://github.com/mailhog/MailHog/releases/download/v1.0.1/MailHog_linux_amd64
sudo chmod +x /usr/local/bin/mailhog

sudo tee /etc/systemd/system/mailhog.service <<EOF > /dev/null
[Unit]
Description=MailHog Service
After=network.service vagrant.mount

[Service]
Type=simple
ExecStart=/usr/bin/env /usr/local/bin/mailhog

[Install]
WantedBy=multi-user.target
EOF

sudo systemctl daemon-reload
sudo systemctl enable mailhog
sudo systemctl restart mailhog

if [ "${PACKAGE_MANAGER}" = "apt" ]
then
  sudo grep -q mailhog /etc/nginx/sites-enabled/nginx-passbolt.conf || sudo sed -i '/^.*index index.php/a \ \ include /etc/nginx/snippets/mailhog.conf;' /etc/nginx/sites-enabled/nginx-passbolt.conf
elif [ "${PACKAGE_MANAGER}" = "yum" ] || [ "${PACKAGE_MANAGER}" = "dnf" ] || [ "${PACKAGE_MANAGER}" = "zypper" ]
then
  sudo grep -q mailhog /etc/nginx/conf.d/passbolt.conf || sudo sed -i '/^.*index index.php/a \ \ include /etc/nginx/snippets/mailhog.conf;' /etc/nginx/conf.d/passbolt.conf
  if [ -f /etc/nginx/conf.d/passbolt_ssl.conf ]
  then
    sudo grep -q mailhog /etc/nginx/conf.d/passbolt_ssl.conf || sudo sed -i '/^.*index index.php/a \ \ include /etc/nginx/snippets/mailhog.conf;' /etc/nginx/conf.d/passbolt_ssl.conf
  fi
fi

sudo mkdir -p /etc/nginx/snippets
sudo tee /etc/nginx/snippets/mailhog.conf << EOF > /dev/null
location /webmail/ {
  proxy_pass http://127.0.0.1:8025/;
  chunked_transfer_encoding on;
  proxy_set_header X-NginX-Proxy true;
  proxy_set_header Upgrade \$http_upgrade;
  proxy_set_header Connection "upgrade";
  proxy_http_version 1.1;
  proxy_redirect off;
  proxy_buffering off;
}
EOF

sudo systemctl reload nginx.service
