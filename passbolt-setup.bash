#!/usr/bin/env bash

set -euo pipefail

unset PASSBOLT_SELENIUM_ACTIVE
unset DEBUG

LC_ALL="en_US.UTF-8"
LC_CTYPE="en_US.UTF-8"
export ARGUMENT=${1:-}
export SCHEME=http

# shellcheck source=/dev/null
source /etc/os-release

SCRIPT_DIR=$(cd -- "$(dirname -- "$0")" && pwd -P)
cd "${SCRIPT_DIR}"

# shellcheck source=/dev/null
. lib/functions.bash
# shellcheck source=/dev/null
. lib/configuration.bash
# shellcheck source=/dev/null
. lib/os-detect.bash
# shellcheck source=/dev/null
. lib/parser.bash

compliance_check

check_subscription_key
configure_ssl_keys

install_dependencies
setup_repository
install_passbolt

if [ "${MAILHOG_ENABLE:-false}" = "true" ]
then
  # shellcheck source=/dev/null
  . lib/mailhog-setup.bash
fi

# shellcheck source=/dev/null
. lib/passbolt-generate-gpg.bash
# shellcheck source=/dev/null
. lib/generate-passbolt-config.bash

copy_subscription_key
configure_first_admin
